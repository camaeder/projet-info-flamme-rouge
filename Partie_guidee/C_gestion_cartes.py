from random import *
import random

######### SECTION : GESTION DES CARTES #################
########################################################

#Cette fonction donne le paquet initial détenu par chaque joueur
def paquet_initial():
    # ce paquet est initialement prévu pour une partie à 5 lignes,
    # des segments de 20 cases, un start = 4 et terminus = 10
    return [2] * 3 + [3] * 3 + [4] * 3 + [5] * 3 + [6] * 3 + [9] * 3

#Il va falloir stocker tout au long de la partie le paquet de cartes de chaque joueur
#Pour cela, on va manipuler un dictionnaire qui admettra les noms des joueurs
#comme clés et leur paquet comme valeur.
#Au début de la partie, les joueurs auront des paquets équivalents, mais ceux-ci évolueront
#au fur et à mesure de la partie, en fonction des déplacements de chacun.
#Cette fonction prend en entrée la liste des joueurs et renvoie le dictionnaire.
def dico_paquets_initial(liste_joueurs):
    dico_paquets = {}
    for joueurs in liste_joueurs :
        dico_paquets[joueurs]=paquet_initial()
    return dico_paquets

#Chaque joueur possède aussi une défausse contenant les cartes déplacements
#qu'il n'a pas choisi. Lorsque la défausse sera vide, on la mélange et remet son contenu
#dans le paquet. Au début de la partie, la défausse est vide.
#Cette fonction crée donc un dictionnaire où les clés sont les joueurs et les valeurs
#sont les défausses initiales (à savoir des listes vides)
def dico_defausses_initial(liste_joueurs):
    dico = {}
    for joueur in liste_joueurs:
        dico[joueur] = []
    return dico

#Fonction configuration_joueur
#Elle prend en argument liste_joueur, qui est une liste des joueurs qui participent à la partie
#Pour chacun des joueurs de la liste l'interface demande d'entrer la configuration du joueurs, elle crée ensuite un dictionnaire avec les joueurs en clés et leur configuration en valeur que la fonction renvoie
def configuration_joueurs(liste_joueur):
    dico_configuration = {}
    for joueur in liste_joueur :
        config = ""
        print ("Quel mode pour le joueur ", joueur, " : Aleatoire ou Manuel ? (A/M)")
        config = input("")
        while (config != 'a' and config != 'A' and config != 'M' and config != 'm'):
            print ("Recommencez ! Quel mode pour le joueur ", joueur, " : Aleatoire ou Manuel ? (A/M)")
            config = input("")
        if config == 'a':
            dico_configuration [joueur] = 'A'
        elif config == 'm' :
            dico_configuration [joueur] = 'M'
        else :
            dico_configuration [joueur] = config
    print ("Choix des configurations effectué !")
    return dico_configuration

#Fonction choix_carte_alea
#Elle prend en argument main_joueur, qui est une liste d'entier qui represente la main d'un joueur
#Elle renvoie une carte au hasard dans la main de ce joueur
def choix_carte_alea (main_joueur):
    return choice (main_joueur)

#Fonction choix_cartes_manuel
#Elle prends en argument nom_joueur qui est une chîne de caractère qui correspond au nom du joueur a qui est adressé la question et main_joueur qui est une liste d'entier qui correspond a toutes les cartes déplacement que le joueur possèdent
#Elle renvoie la carte que le joueur a choisi, un entier
def choix_carte_manuel (nom_joueur, main_joueur) :
    print("Quel déplacement pour", nom_joueur," souhaitez-vous effectuer ? ",main_joueur)
    carte=int(input(""))
    while carte not in main_joueur :
        print("Recommencez ! Quel déplacement pour", nom_joueur," souhaitez-vous effectuer ? ",main_joueur)
        carte=int(input(""))
    return carte

#Fonction pioche_main_joueur
#Elle prend en argument nom_joueur, une chaîne de caractère qui correspond au nom du joueur qui pioche, dico_paquets qui est le dictionnaire qui contient les paquets de tous les joueurs de la partie, et dico_defausses qui est un dictionnaire qui contient toutes les defausses de tous les joueurs de la partie (pour les deux dictionnaires, les clés sont le nom des joueurs et les valeurs sont la liste des cartes qui composent leurs paquets ou defausses)
#Elle renvoie la liste des cartes que le joueur a tiré, elle modifie aussi le paquet du joueur en enlevant les cartes tirées ainsi que la defausse du joueur ( si le joueur n'avait pas assez de carte dans son paquet)
def pioche_main_joueur (nom_joueur,dico_paquets,dico_defausses):
    liste_renvoie = []
    paquet_joueur = dico_paquets[nom_joueur]
    if len(paquet_joueur) >= 4 :
        for _ in range (4):
            liste_renvoie.append(paquet_joueur[0])
            paquet_joueur.pop(0)
        dico_paquets[nom_joueur]=paquet_joueur
        return liste_renvoie
    else :
        for _ in range (len(paquet_joueur)):
            liste_renvoie.append(paquet_joueur[0])
            paquet_joueur.pop(0)
        paquet_joueur=dico_defausses[nom_joueur]
        random.shuffle(paquet_joueur)
        dico_defausses[nom_joueur]=[]
        for _ in range (4- len(liste_renvoie)) :
            liste_renvoie.append(paquet_joueur[0])
            paquet_joueur.pop(0)
        dico_paquets[nom_joueur]=paquet_joueur
        return liste_renvoie 

#Fonction selection_carte_main
#Elle prend en argument nom_joueur qui est une chaîne de caraactère qui correspond au nom du joueur concerné, main_joueur qui est une liste de toutes les cartes piochées par ce joueur, choix qui est entier qui correspond à la carte choisi par le joueur et ddico_defausses qui est le dictionnaire qui contient les défausses de tout les joueurs
#Elle ajoute les cartes que le joueur n'as pas choisi de jouer a sa défausse
#Elle ne renvoie rien
def selection_carte_main (nom_joueur, main_joueur, choix, dico_defausses):
    main_joueur.remove(choix)
    for carte in main_joueur :
        dico_defausses[nom_joueur].append(carte)

#Fonction choix_deplacement
#Elle prend en argument nom_joueur qui est le nom du joueur concerné donc un chaîne de caactère, dico_paquets qui est le dictionnaire de tous les paquets de tous les joueurs de la partie, dico_defausses qui est le dictionnaire qui contient toutes les défausses de tout les joueurs, 
#configuration qui est le dictionnaire qui répertorie le choix de configurations de tous les joueurs de la partie (les trois dictionnaires ont pour clés le nom des joueurs et pour valeurs le paquet / la défausse / le choix de la configuration de ce joueur) et affichage qui est un booléen qui précise si le joueur veut afficher le contenus de son paquet et de sa défausse
#Elle toutes les actions qu'entraine le tour d'un joueur (quand un joueur joue elle lui donne sa main lui demande ce qu'il veut jouer (si il est en mode de jeu manuel) et modifie dico_paquets et dico_defausses en fonction de son choix)
#Elle renvoie la carte que le joueur a choisi
def choix_deplacement (nom_joueur, dico_paquets, dico_defausses, configurations, affichage = False) :
    pioche = pioche_main_joueur(nom_joueur, dico_paquets, dico_defausses)
    carte_choisi = 0
    if configurations[nom_joueur]=='A' :
        carte_choisi = choix_carte_alea (pioche)
    else :
        carte_choisi = choix_carte_manuel(nom_joueur, pioche)
    selection_carte_main(nom_joueur, pioche, carte_choisi, dico_defausses)
    if affichage == True :
        print ("Le paquet du joueur",nom_joueur,"contient :",dico_paquets[nom_joueur])
        print ("La défausse du joueur",nom_joueur,"contient :",dico_defausses[nom_joueur])
    return carte_choisi

