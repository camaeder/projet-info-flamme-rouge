from D_deplacement_coureurs import *

######### SECTION : PHASE TERMINALE DU JEU #################
##############################################################

#Fonction distribution_fatigue
#Elle prend en argument dico_positions un dictionnaire qui contient les positions de tous les joueurs, positions_droite la liste des positions droites de la course, et dico_defausses le dictionnaire qui contient les défausses des joueurs
#Cette fonction regarde pour tous les joueurs de la partie si la case devant eux est occupée si ce n'est pas le cas, elle donne une carte 2 en plus dans leur defausse
#Elle ne renvoie rien
def distribution_fatigue (dico_positions, positions_droite,dico_defausses) :
    for joueur in dico_positions :
        position_joueur = int(dico_positions[joueur][1:])
        if positions_droite[position_joueur + 1] == '__' :
            dico_defausses[joueur].append(2)

#Cette fonction détermine les différents groupes de coureurs,
#qui sont séparés au moins par une case complètement vide
#Elle renvoie donc une liste de listes, dont le premier élément
#est le groupe étant le plus proche de la ligne de départ
# (donc le plus en retard dans la course)
#Dans chaque groupe, les coureurs sont aussi classés du plus en retard
#au plus proche de la ligne d'arrivée
def groupes_coureurs(dico_positions,positions_droite,positions_gauche):
    joueur_min = le_moins_eloigne(dico_positions,positions_droite)
    joueur_max = le_plus_eloigne(dico_positions)
    case_min = numero_de_case(joueur_min,dico_positions)
    case_max = numero_de_case(joueur_max,dico_positions)
    i = case_min
    liste_groupes = []
    while i <= case_max:
        groupe = []
        droite_i = positions_droite[i]
        gauche_i = positions_gauche[i]
        while droite_i != "__":
            if gauche_i != "__":
                groupe.append(gauche_i)
            groupe.append(droite_i)
            i += 1
            droite_i = positions_droite[i]
            gauche_i = positions_gauche[i]
        if len(groupe) > 0:
            liste_groupes.append(groupe)
        i += 1
    return liste_groupes

#Fonction decale_coureurs
#Elle prend en argument groupe qui est une liste de joueurs faisant partie du même groupe de coureurs (tiré en partant du joueurs le plus loin de l'arrivée), dico_positions un dictionnaire qui contient les positions de tous les joueurs, positions_droites et positions_gauches qui sont les deuc listes de positions de la course
#Cette fonction décale tous les coureurs d'un même groupe d'une case de plus
#Elle ne renvoie rien
def decale_coureurs (groupe, dico_postions, positions_droite, positions_gauche) :
    inverse_groupe = reversed(groupe)
    for joueur in inverse_groupe : 
        avancee_coureurs(joueur, dico_postions, positions_droite, positions_gauche,1)

#Fonction aspiration
#Elle prend en argument dico_positions un dictionnaire de la position de tous les joueurs, positions_droite et positions_gauche les deux listes de postions de la course
#Cette fonction regarde pour tous les groupe de joueurs de la partie si il peuvent bénficier de l'effet d'aspiration
#Elle ne renvoie rien 
def aspiration (dico_positions, positions_droite, positions_gauche) :
    groupes = groupes_coureurs(dico_positions,positions_droite,positions_gauche)
    indice = 0
    element = []
    while len(groupes) != indice :
        element = groupes[indice]
        if positions_droite[int(dico_positions[element[len(element)-1]][1:]) + 2] != '__' :
            decale_coureurs(element,dico_positions, positions_droite, positions_gauche)
            for element in groupes[indice + 1] :
                groupes [indice].append(element)
            groupes.pop(indice + 1)
        else :
            indice += 1
#Fonction tour_de_jeu
#Elle prend en arguments : dico_positions qui est un dictionnaire qui contient les positions de tous les joueurs, positions_droite et positions_gauche les deux listes de positions de la course, dico_paquets qui est un dictionnaire qui contient les paquets de tous les joueurs de la partie,
#dico_defausses qui est un dictionnaire qui contient toutes les défausses des joueurs de la partie, configurations qui est un dictionnnaire qui contient les configuratio des joueurs et affichage un booléen qui précise si l'utilisateur veut les affichages optionnels
#Cette fonction génére un tour de jeu avec les effets de jeu
#Elle ne renvoie rien
def tour_de_jeu (dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage) :
    tour_de_jeu_sans_effet (dico_positions,positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage)
    distribution_fatigue (dico_positions, positions_droite, dico_defausses)
    aspiration (dico_positions, positions_droite, positions_gauche)

