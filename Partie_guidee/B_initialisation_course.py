from random import *

######### SECTION : INITIALISATION DE LA COURSE ##########
##########################################################

#Fonction creer_liste_coureurs
#Elle prends en arguments un entiers nb_joueurs (qui est censé être strictement positif) 
#Elle renvoie la liste des coureurs

def creer_liste_coureurs(nb_joueurs) :
    if nb_joueurs<=0 :
        print ("Le nombre de joueurs doit être positif")
        return ""
    else :
        liste_coureurs=[]
        for indice in range (nb_joueurs) :
            indice_str= str(indice)
            liste_coureurs.append("J"+indice_str)
        return liste_coureurs

#Fonction creer_listes_positions
#Elle prend en argument deux entiers, nb_lignes (qui corresponds au nombre de route du parcours) et taille_seg (qui corresponds au nombre de case que contient chaque route)
#Elle renvoie deux listes de chaîne de caractère de taille identique, l'élément "__" qui compose les listes symbolyse une case vide sur le parcours 

def creer_liste_positions (nb_lignes,taille_seg):
    liste1=[]
    liste2=[]
    for _ in range (nb_lignes*taille_seg) :
        liste1.append("__")
        liste2.append("__")
    return liste1, liste2

#Fonction placer_joueur_alea
#Elle prend en argument une liste de toutes les positions disponible, positions_dispos
#Elle renvoie un element aleatoire de la liste et supprime cette element de positions_dispos

def placer_joueurs_alea (positions_dispos):
    if positions_dispos==[] :
        print("Il n'y a plus de positions disponible")
        return ""
    else :
        pos_choisi=choice(positions_dispos)
        positions_dispos.remove(pos_choisi)
        return pos_choisi
    
#Fonction initialiser_jeu_alea
#Elle prends en argument 3 listes, liste_joueurs qui contient la liste des coureurs de la partie, positions_droite et positions_gauche qui correspondent au listes de positions droite et gauche, et un entier start qui donne l'emplacement de la ligne de départ
#Elle ne renvoie rien, mais elle modifie les listes positions_droite et positions_gauche pour y placer les joueurs que contient liste_joueurs

def initialiser_jeu_alea (liste_joueurs,positions_droite,positions_gauche,start):
    if len(liste_joueurs)>start*2 :
        print("Il plus de joueurs que de place avant le départ, veuillez modifier le nombre de joueurs ou la position du départ")
    else :
        liste_positions=[]
        for indice in range (start) :
            indice_str=str(indice)
            liste_positions.append("D"+indice_str)
            liste_positions.append("G"+indice_str)
        for joueur in liste_joueurs :
            position=placer_joueurs_alea(liste_positions)
            numero=int(position [1:])
            if position[:1] == "D" :
                positions_droite.pop(numero)
                positions_droite.insert(numero,joueur)
            else:
                positions_gauche.pop(numero)
                positions_gauche.insert(numero,joueur)          

#Fonction construit_dico_position
#Elle prend en argument les listes positions_droite et positions_gauche(les deux listes qui correspondents au placement des joueurs) et l'entier start qui indique l'emplacement de la ligne de départ
#Elle renvoie le dicitionnaire qui associe les joueurs a leurs positions (les clés étant le noms des joueurs et les valeurs leur position)

def construit_dico_positions (positions_droite,positions_gauche,start):
    dico_positions={}
    for indice in range (start):
        if positions_droite[indice]!="__":
            dico_positions[positions_droite[indice]] = "D" + str(indice)
        if positions_gauche[indice]!="__":
            dico_positions[positions_gauche[indice]] = "G" + str(indice)
    return dico_positions

#Fonction numero_de_case
#Elle prend en argument une chaîne de caractere,le nom d'un joueur, ainsi que le dictionnaire des positions, dico_positions
#Elle renvoie l'indice de la case où se trouve le joueur passé en argument

def numero_de_case(nom_joueur,dico_positions):
    return int(dico_positions[nom_joueur][1:])




