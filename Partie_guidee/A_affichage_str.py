
######### SECTION : AFFICHAGE DU JEU ##########
###############################################

#Fonction ligne_vers_droite
#Pour représenter une ligne de route, où les coureurs se déplacent vers la droite
#positions est une liste contenant des caractères à afficher

#Cette fonction renvoie une chaîne de caractère qui represente une ligne de la route où les joureurs avancent quand ils se deplacent vers la droite

def ligne_vers_droite (position):  
    ligne_route=">"
    for element in (position) :
        ligne_route+=" " + element + " |"
    return ligne_route 


#Fonction route_vers_droite
#Une route est constituée de deux lignes en parallèle, pour représenter les deux voies.
#Chaque case est constituée de deux côtés, droite et gauche.
#positions_droite et positions_gauche sont des listes de même longueur contenant des caractères

#Cette fonction renvoie une chaîne de caractère qui represente la route où les joueurs avancent quand ils se déplacent vers la droite

def route_vers_droite (positions_droite,positions_gauche):
    if len(positions_droite)!=len(positions_gauche):
        return ("Erreur, les listes de positions doivent avoir la même taille.")
    else :
        return ligne_vers_droite (positions_gauche) + "\n" + ligne_vers_droite(positions_droite)




#Fonction inverse
#On souhaite maintenant créer des routes, où les coureurs se dirigent vers la gauche
#Cette fonction inverse une ligne, en enlevant le dernier élément et le remplaçant par "<"
def inverse(ligne):
    return ligne[:0:-1] + "<"



#Fonction route_vers_gauche
#Cette fonction renvoie une route où les coureurs se dirigent vers la gauche.

#Cette fonction renvoie une chaîne de caractère qui represente la route où les joueurs avancent quand ils se déplacent vers la gauche

def route_vers_gauche (positions_droite,positions_gauche):
    if len(positions_droite)!=len(positions_gauche):
        return ("Erreur, les listes de positions doivent avoir la même taille.")
    else :
        indice = 0
        for element in positions_droite :
            if element != '__' :
                positions_droite[indice] = element [::-1]
            indice += 1
        indice = 0
        for element in positions_gauche :
            if element != '__' :
                positions_gauche[indice] = element[::-1]
            indice += 1
        return inverse(ligne_vers_droite (positions_droite)) + "\n" + inverse(ligne_vers_droite(positions_gauche))       




#Fonction ligne_depart
#Les routes de départ sont forcément vers la droite par défaut.
#Elles commencent par ">>>>>" au lieu de ">"
#Une zone de start est présente au début et termine avec un "X" au lieu de "|"
    
#Cette fonction renvoie une chaîne de caractère qui représente une ligne de la ligne de départ où les coureurs avancent lorsqu'ils se déplacent vers la droite, le départ est représenter par un X
    
def ligne_depart (positions,start) :
    route=">>>>>" 
    if start==0:
        route += "  "
    if start != int(start) :
        return ("Erreur, start doit être un nombre entier.")
    elif len(positions) < start :
        for element in (positions):
            route+=" " + element + " |"
        return route
    else :
        for indice in range (start):
            route += " " + positions [indice] + " |"
        route = route [:-1]
        route += 'X'
        for indice in range (len(positions)-start):
            route += " " + positions [indice + start] + " |"
        return route




#Fonction route_depart
#Cette fonction représente une route de départ avec deux lignes de départ en parallèle

#Cette fonction renvoie une chaîne de caractère qui représente la route de la ligne de départ, où les coureurs avancent lorsqu'ils se déplacent vers la droite, le départ est représenter par un X
    
def route_depart (positions_droite,positions_gauche,start):
    if len (positions_droite) != len(positions_gauche):
        return ("Erreur, les listes de positions doivent avoir la même longueur.")
    else:
        return ligne_depart(positions_gauche,start) + "\n" + ligne_depart(positions_droite,start)




#Fonction ligne_arrivee_vers_droite
#On représente désormais une ligne d'arrivée où les coureurs se dirigent vers la droite
#Un entier terminus > 1 indique l'emplacement de la ligne d'arrivee pour gagner la course

#Cette fonction renvoie une chîne de caractère qui représente la une ligne de la ligne d'arrivée de la course, representée par XXXX, lorsque les joueurs avancent vers la droite

def ligne_arrivee_vers_droite (positions,terminus):
    route = ">"
    if terminus != int(terminus):
        return ("Erreur, terminus doit être un nombre entier")
    elif len(positions)<terminus:
        for element in (positions):
            route += " " + element + " |"
        return route
    else:
        for indice in range (terminus):
            route += " " + positions[indice] + " |"
        route += "XXXX|"
        for indice in range (len(positions)-terminus):
            route += " " + positions[indice+terminus] + " |"
        return route




#Fonction route_arrivee_vers_droite
##Cette fonction représente une route de départ avec
#deux lignes d'arrivee en parallèle

#Cette fonction renvoie Une chaîne de caractère qui représente la route de la ligne d'arrivée, cette ligne étant symbolisee par XXXX, quand les joueurs avancent vers la droite 

def route_arrivee_vers_droite (positions_droite,positions_gauche,terminus):
    if len(positions_droite)!=len(positions_gauche):
        return ("Erreur, les deux listes de positions doivent avoir la même longueur")
    else :
        return ligne_arrivee_vers_droite (positions_gauche,terminus) + "\n" + ligne_arrivee_vers_droite (positions_droite,terminus)


#Fonction route_arrivee_vers_gauche
#Même chose que route_arrivee_vers_droite sauf que cette fois_ci
#les coureurs se déplacent vers la gauche

#Cette fonction renvoie Une chaîne de caractère qui représente la route de la ligne d'arrivée, cette ligne étant symbolisee par XXXX, quand les joueurs avancent vers la gauche

def route_arrivee_vers_gauche (positions_droite,positions_gauche,terminus):
    if len(positions_droite)!=len(positions_gauche):
        return ("Erreur, les deux listes de positions doivent avoir la même longueur")
    else :
        return inverse(ligne_arrivee_vers_droite(positions_droite,terminus)) + "\n" + inverse(ligne_arrivee_vers_droite(positions_gauche,terminus))




#Fonction affichage_route
#Etant donné un nombre de lignes, la longueur de chaque ligne (taille_seg),
#deux liste de positions (pour la droite et la gauche),
#on affiche la route avec les caractéristiques indiquées et le contenu des listes
#de positions. L'emplacement du départ sur la première ligne (start) et de l'arrivée
#sur la dernière (terminus) sont donnés comme arguments optionnels
#On suppose nb_lignes >= 2

def affichage_route (nb_lignes,taille_seg,positions_droite,positions_gauche,start=4,terminus=10):
    i = taille_seg
    if nb_lignes<2 :
        return ("Erreur, le nombre de ligne doit être égal ou superieur à 2")
    elif len(positions_gauche)!=len(positions_droite):
        ("Erreur, les deux listes de positions doivent avoir la même longueur")
    elif len(positions_droite)!=nb_lignes*taille_seg or len(positions_gauche)!=nb_lignes*taille_seg:
        return ("Erreur, le nombre d'éléments des listes de positions doit être égal au nombre de cases du parcours")
    else :
        route = route_depart (positions_droite[:i],positions_gauche[:i],start) + "\n"
        for indice in range (nb_lignes-2):
            indice+=1
            if indice%2!=0:
                route += route_vers_gauche(positions_droite[i:i+taille_seg],positions_gauche[i:i+taille_seg]) + "\n"
            else :
                route += route_vers_droite(positions_droite[i:i+taille_seg],positions_gauche[i:i+taille_seg]) + "\n"
            i += taille_seg
        if nb_lignes%2==0:
            route += route_arrivee_vers_gauche(positions_droite[i:i+taille_seg],positions_gauche[i:i+taille_seg],terminus)
        else :
            route += route_arrivee_vers_droite(positions_droite[i:i+taille_seg],positions_gauche[i:i+taille_seg],terminus) 
        return route

    






