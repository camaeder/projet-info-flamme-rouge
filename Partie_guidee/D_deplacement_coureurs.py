from C_gestion_cartes import *
from B_initialisation_course import *
from A_affichage_str import *

######### SECTION : DEPLACEMENT DES COUREURS #################
##############################################################

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus loin
#du start dans la course.
#Elle prend en entrée le dictionnaire des positions
def le_plus_eloigne(dico_positions):
    max_case = -1
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    max_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case > max_case:
            max_joueur = joueur
            max_case = case
    return max_joueur

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus
#en retrait dans la course.
#Elle prend en entrée le dictionnaire des positions et la liste des positions droite
def le_moins_eloigne(dico_positions,positions_droite):
    min_case = len(positions_droite)
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    min_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case < min_case:
            min_joueur = joueur
            min_case = case
    return min_joueur

#A chaque tour, le premier coureur à se déplacer est celui qui est en tête de
#la course, c'est-à-dire situé sur la plus haute case possible et à droite
#Cette fonction renvoie la liste des coureurs par ordre de placement sur la course
#Le coureur le plus proche de l'arrivée est situé en premier
def tri_joueurs(dico_positions):
    copie_dico = dict(dico_positions)
    ordre_joueurs = []
    while len(copie_dico) > 0:
        prochain_joueur = le_plus_eloigne(copie_dico)
        ordre_joueurs.append(prochain_joueur)
        del copie_dico[prochain_joueur]
    return ordre_joueurs

#Fonction avancee_coureurs
#Elle prend en arguments : nom_joueur qui est une chaîne de caractère, dico_positions le dictionnaire qui contient toutes les positions de tous les joueurs, positions_droite et positions_gauche deux dictionnaires qui contiennent toutes les positions du jeu et distance un entier qui corresspond aux nombre de cases dont le joueur avance
#Cette fonction met à jour toutes les données pour faire en sorte que le joueur parcoure le nombre de case souhaité (tout en respectant les règles du jeu)
#Elle ne renvoie rien
def avancee_coureurs (nom_joueur, dico_positions, positions_droite, positions_gauche, distance) :
    compteur = 0
    position_initiale_joueur = int(dico_positions[nom_joueur][1:])
    if (dico_positions[nom_joueur][:1]) == 'D' :
        positions_droite[position_initiale_joueur] = '__'
    else :
        positions_gauche[position_initiale_joueur] = '__'
    liste_pos = positions_droite
    pos_droite = 0
    pos_gauche = 0
    pos_joueur = pos_droite
    while liste_pos[pos_joueur + distance + position_initiale_joueur] != "__" :
        if compteur % 2 == 0 :
            pos_droite -= 1
            liste_pos = positions_gauche
            pos_joueur = pos_gauche
        else :
            pos_gauche -= 1
            liste_pos = positions_droite
            pos_joueur = pos_droite
        compteur += 1
    if compteur % 2 == 0 :
        positions_droite[pos_joueur + distance + position_initiale_joueur] = nom_joueur
        dico_positions[nom_joueur] = "D" + str(pos_joueur + distance + position_initiale_joueur)
    else : 
        positions_gauche[pos_joueur + distance + position_initiale_joueur] = nom_joueur
        dico_positions[nom_joueur] = "G" + str(pos_joueur + distance + position_initiale_joueur)

#Fonction premiere_case_apres_arrivee
#Elle prend en argument nb_ligne un entier qui correspond au nombre de segments de la route, taille_seg qui est un entier qui correspond à la taille de tous les segments de la route et terminus qui est un entier qui correspond à la position de la ligne d'arrivée sur le dernier segments
#Elle renvoie l'indice de la première case située apres l'arrivée dans les listes positions_droite et positions_gauche (un entier)
def premiere_case_apres_arrivee (nb_lignes, taille_seg, terminus):
    return terminus + ((nb_lignes - 1)*taille_seg)

#Fonction test_victoire
#Elle prend en arguments nb_lignes un entier qui correspond au nombre de segments de la route, taille_seg qui est un entier qui correspond à la taille de tous les segments de la route, 
#dico_positions qui est le dictionnaire qui repertorie les positions de tous les joueurs(le nom du joueurs étant la clés et sa position la valeur) et terminus qui est un entier qui correspond à la position de la ligne d'arrivée sur le dernier segment
#Elle verfie si un des joueurs a passé la ligne d'arrivée et renvoie True si c'est le cas ou False (elle renvoie donc un booléen)
def test_victoire (nb_lignes, taille_seg, dico_positions, terminus):
    un_gagnant = False
    for joueurs in dico_positions :
        if int(dico_positions[joueurs][1:]) >= premiere_case_apres_arrivee (nb_lignes, taille_seg, terminus):
            un_gagnant = True
    return un_gagnant

#Fonction tour_de_jeu_sans_effet
#Elle prend en argument dico_positions un dictionnaire qui contient toutes les positions des joueurs, positions_droite et positions_gauche qui sont les deux listes de positions de la course, dico_paquets un dictionnaire qui contient les paquets des joueurs,
#dico_defausses un dictionnaire qui contient les defausses des joueurs, configurations un dictionnaire qui contient les configurations des joueurs, et affichage un booléen qui precise si l'utilisateur veux afficher les commentaires optionnels qui précise les débuts et les fins de tour et aussi de combien de cases les joueurs avance 
#Cette fonction simule un tour de jeu sans les effets (elle demande le choix des joueurs pour ce tour et les applique)
#Elle ne renvoie rien
def tour_de_jeu_sans_effet (dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses,configurations, affichage = True):
    if affichage == True :
        print("##### Début du tour #####")
    choix_joueurs = {}
    for joueurs in dico_positions :
        choix_joueurs[joueurs]=choix_deplacement(joueurs, dico_paquets,dico_defausses,configurations,affichage)
        if affichage == True :
            print ("Le joueur",joueurs,"se deplace de",choix_joueurs[joueurs])
    positions_joueurs = []
    copie_dico_positions = dico_positions.copy()
    for elements in dico_positions :
        maxi = -1
        memoire = ''
        memoire_joueurs = ''
        for joueurs in copie_dico_positions :
            if int(copie_dico_positions[joueurs] [1:]) > maxi :
                maxi = int(copie_dico_positions[joueurs] [1:])
                memoire = copie_dico_positions[joueurs][:1]
                memoire_joueurs = joueurs
            elif int(copie_dico_positions[joueurs] [1:]) == maxi :
                if memoire == 'G' :
                    maxi = int(copie_dico_positions[joueurs] [1:])
                    memoire = 'D'
                    memoire_joueurs = joueurs
        positions_joueurs.append(memoire_joueurs)
        del copie_dico_positions[memoire_joueurs]
    for joueur in positions_joueurs :
        if int(dico_positions[joueur][1:]) + choix_joueurs[joueur] >= len(positions_droite) :
            print (joueur,"à gagné !")
            break
        else :
            avancee_coureurs(joueur,dico_positions,positions_droite,positions_gauche,choix_joueurs[joueur])
    if affichage == True :
        print ("##### Fin du tour #####")


