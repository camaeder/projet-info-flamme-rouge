#Ce fichier contient toutes les fonctions permetant de choisir son velos et sa map

from random import *


#Fonction choix_velo
#Cette focntion prend en argument liste_joueurs qui est une liste contenant les noms de tous les joueurs, et configuration le dictionnaire des configurations
#Elle demande a chaque joueur de choisir un vélo et renvoie un dicotionnaire ayant comme clés le nom des joueurs et pour valeur leur vélo.
def choix_velos (liste_joueurs,configuration) :
    dico_velo = {}
    for joueur in liste_joueurs :
        if configuration[joueur]=='A' :
            dico_velo[joueur] = randint(1,4)
        else :
            velo = 0
            print("Choisissez votre vélo (entrez le chiffre associé) : vélo de ville (1), VTT (2), Tandem (3), vélo des sables (4)")
            velo = int(input(""))
            while velo != 1 and velo != 2 and velo !=3 and velo != 4 :
                print ("Cette valeur n'est pas valide, vous avez que le choix entre ces 4 vélos : vélo de ville (1), VTT (2), Tandem (3), vélo des sables (4). Entrez le numéro du vélo")
                velo = int(input(""))
            dico_velo[joueur]=velo
    return dico_velo

#Fonction saut_tour 
#Cette fonction renvoie un booléen, elle determine de manière aléatoire si oui ou non, un tour va être sauté
def saut_tour () :
    res = (0,1)
    if res == 0 :
        return True
    else : 
        return False

#Fonction multiple_virage
#Cette fonction prend comme argument nbr_a_multi et nbr_de_fois edux entiers
#elle renvoie la liste des nbr_de_fois premier multiple de nbr_a_multi (à noter quue tout ces multiples sont additionné avec 1)
def multiple_virage (nbr_a_multi,nbr_de_fois) :
    liste = []
    for multi in range (1,nbr_de_fois + 1) :
        liste.append((nbr_a_multi*multi)+1)
    return liste

#Fonction bonus_malus_virage
#Elle prend comme argument nb_lignes un entier qui correspond au nombre de ligne que contient le circuit, taille_seg qui est un entier qui correspond a la taille d'un segement du circuit, 
#carte_jouee  qui est un dictionnaire qui a pour clés le nom des joueurs et pour valeur la carte qu'a jouer la carte, dico_positions le dicitionnaire qui contient les positions de tous les joueurs, 
#dico_defausses qui est un dictionnaire contient toutes les défausses des joueurs et dico_velo un dictionnaire qui contient les vélos de tous les joueurs
#Cette fonction distribue les bonus et les malus qui ont un rapport avec les virage, elle ne renvoie rien
def bonus_malus_virage (nb_lignes,taille_seg, carte_jouee, dico_positions,dico_defausses, dico_velo) :
    pos_apres_virage = taille_seg + 1
    pos_joueur = 0
    liste_multiple = multiple_virage(taille_seg,nb_lignes)
    for joueur in dico_positions :
        if dico_velo[joueur] == 2 or dico_velo[joueur] == 4 :
            pos_joueur = int(dico_positions[joueur][1:])
            for i in range (0,len(liste_multiple)) : 
                if ((pos_joueur - carte_jouee[joueur]) < liste_multiple[i]) and (pos_joueur >= liste_multiple[i]) :
                    if dico_velo[joueur] == 2 :
                        dico_defausses[joueur].append(6)
                    else :
                        dico_defausses[joueur].append(2)

#Fonction bonus_malus_tandem
#Elle prends pour argument nbr_tour qui est un entier qui correspond au nombre de tour qui a déjà été joueur, dico_defausses le dictionnaire qui contient toutes les défausses et dico vélo le dictionnaire qui contient tous les vélos des joueurs
#Elle distribue les bonus et les malus lié aux tandems, et elle ne renvoie rien
def bonus_malus_tandem (nbr_tour,dico_defausses,dico_velo) :
    for joueur in dico_velo :
        if dico_velo[joueur] == 3 :
            if nbr_tour%2 != 0 :
                dico_defausses[joueur].append(6)
            else :
                dico_defausses[joueur].append(2)

#Fonction bonus_sable
#Elle prends pour argument nbr_tour qui est un entier qui correspond au nombre de tour qui a déjà été joueur, dico_defausses le dictionnaire qui contient toutes les défausses et dico vélo le dictionnaire qui contient tous les vélos des joueurs
#Elle distribue les bonus lié au vélo des sables, et elle ne renvoie rien
def bonus_sable (nbr_tour,dico_defausses,dico_velo) :
    for joueur in dico_velo :
        if dico_velo[joueur] == 4 and nbr_tour%2 != 0 :
            dico_defausses[joueur].append(4)

#Fonction choix_map
#Cette fonction demande à l'utilisateur sur quelle map il veut jouer et renvoie le numéro qui lui est associé
def choix_map () :
    map = 0
    print("Choisissez la map (entrez le chiffre associé) : 'Campagne' (1), 'Plage' (2), 'Montagne' (3), 'Ville' (4). \nSi vous voulez une map au hasard entrez 5")
    map = int(input(""))
    while map != 1 and map != 2 and map !=3 and map != 4 and map !=5:
        print ("Cette valeur n'est pas valide, vous avez que le choix entre ces 4 maps : 'Campagne' (1), 'Plage' (2), 'Montagne' (3), 'Ville' (4). Entrez le numéro de la map que vous souhaitez\nSi vous voulez une maps au hasard, entrez 5")
        map = int(input(""))
    if map == 5 :
        liste_maps = ['Campagne','Plage','Montagne','Ville']
        map = randint(1,4)
        print("Vous jourez sur la maps",liste_maps[map-1],"!")
    return map

#Fonction liste_joueur_ordre
#Elle prend en argument dico_positions le dictionnaire des positions des joueurs et positions_droite la liste de positions de la partie droite de la route
#Elle renvoie la liste des joueur de la course dans l'ordre du plus loin de l'arrivée au plus proche.
def liste_joueur_ordre (dico_positions, positions_droite) :
    liste = []
    copie_dico_positions = dico_positions.copy()
    for elements in dico_positions :
        mini = len(positions_droite) + 1
        memoire = ''
        memoire_joueurs = ''
        for joueurs in copie_dico_positions :
            if int(copie_dico_positions[joueurs] [1:]) < mini :
                mini = int(copie_dico_positions[joueurs] [1:])
                memoire = copie_dico_positions[joueurs][:1]
                memoire_joueurs = joueurs
            elif int(copie_dico_positions[joueurs] [1:]) == mini :
                if memoire == 'D' :
                    mini = int(copie_dico_positions[joueurs] [1:])
                    memoire = 'G'
                    memoire_joueurs = joueurs
        liste.append(memoire_joueurs)
        del copie_dico_positions[memoire_joueurs]
    return liste

#Fonction avalanche
#Elle prend en argument un entier qui correspond a la carte choisi pour cette partie, derniere_avalanche qui est entier qui correspond au nombre de tours qu'il y a eu depuis la dernière avalanche, 
#nbr_tours un entier qui correspond au nombre de tours qu'il y eu depus le début de la partie, dico_positions le dictionnaire des positions des joueurs de la course, positions_droite et positions_gauche les deux listes de positions du circuit
#Cette fonction determine si pendant ce tours il va y avoir une avalanche, si c'est la case les fait les modification nécessaires sur les structure dded jeux pour que tous les joueurs reculent de 6 cases.
#Elle renvoie un entier (derniere_avalanche) qui remet lee compteur de la dernière avalanche à zéro
def avalanche(map,derrniere_avalanche,nbr_tours,dico_positions, positions_droite, positions_gauche):
    if nbr_tours - derrniere_avalanche > 3 and map == 3 and saut_tour() == True :
        print ("Alerte avalenche !!")
        liste_triee = liste_joueur_ordre (dico_positions, positions_droite)
        for joueur in liste_triee :
            lettre = dico_positions[joueur][:1]
            pos = int (dico_positions[joueur][1:])
            compteur = 0
            if lettre == 'D' :
                positions_droite[pos] = '__'
            else :
                positions_gauche[pos] = '__'
            liste_pos = positions_droite
            pos_droite = 0
            pos_gauche = 0
            pos_joueur = pos_droite
            while liste_pos[0 + pos_joueur] != "__" :
                if compteur % 2 == 0 :
                    pos_droite += 1
                    liste_pos = positions_gauche
                    pos_joueur = pos_gauche
                else :
                    pos_gauche += 1
                    liste_pos = positions_droite
                    pos_joueur = pos_droite
                compteur += 1
            if compteur % 2 == 0 :
                positions_droite[0 + pos_joueur] = joueur
                dico_positions[joueur] = "D" + str(0 + pos_joueur)
            else : 
                positions_gauche[0 + pos_joueur] = joueur
                dico_positions[joueur] = "G" + str(0 + pos_joueur)
        return -1     
    return derrniere_avalanche






        
