from A_affichage_str import *
from E_phase_terminale import *
from velo_maps import *
import tkinter as tk

#Affichage de l'entete du jeu dans la console
#Les arguments optionnels peuvent être modifiés pour
#personnaliser son étape !
def entete(numero_etape = 12, depart = "Aurillac", arrivee = "Villeneuve-sur-Lot"):
    chaine = "#" * 100 + "\n"
    chaine += "#"*41 + " LA FLAMME ROUGE " + "#"*41 + "\n"
    chaine += "#" * 100 + "\n" + "\n"
    chaine += "Bienvenue sur l'étape " + str(numero_etape) + " "
    chaine += "du Tour de France 2024 reliant " + depart + " "
    chaine += "à " + arrivee + " ! Bonne chance !!!"
    chaine += "\n" + "\n"
    return chaine

#Affichage de la route dans une fenêtre graphique
def afficher_fenetre(chaine, taille_fen = '600x300+50+10'):
    fen = tk.Tk()
    fen.geometry(taille_fen)
    fen.title("La Flamme Rouge")
    bouton = tk.Button(fen, text="Tour suivant", command=fen.destroy)
    tk.Label(fen, text=chaine).pack()
    bouton.pack(side=tk.LEFT, padx=5, pady=5)
    fen.mainloop()

#Fonction partie_jeu
#Elle prend en argument : nb_joueurs qui est un entier qui est le nombre de joueurs partcipant a la partie, nb_lignes qui est un entier qqui donne le nombre de lignes qu'aura le parcours, taille_seg qui est un entier qui donne la taille qu'aura chaques lignes,
#start qui est un entier qui donne l'emplacement de la ligne de départ, terminus qui donne l'emplacement de la ligne d'arrivée et affichage un booléen qui precise si l'utilasteur veut l'aafichage optionnel
#Cette fonction demmare une partie de jeu
#Elle ne renvoie rien
def partie_jeu (nb_joueurs,nb_lignes,taille_seg,start=4,terminus=10,affichage=False) :
    print(entete())
    liste_joueurs = creer_liste_coureurs(nb_joueurs)
    positions_droite = creer_liste_positions(nb_lignes,taille_seg)[0]
    positions_gauche = creer_liste_positions(nb_lignes,taille_seg)[1]
    configurations = configuration_joueurs(liste_joueurs)
    dico_velo = choix_velos(liste_joueurs,configurations)
    map = choix_map()
    initialiser_jeu_alea(liste_joueurs,positions_droite,positions_gauche,start)
    afficher_fenetre(affichage_route(nb_lignes,taille_seg,positions_droite,positions_gauche,start,terminus))
    dico_positions = construit_dico_positions (positions_droite,positions_gauche,start)
    dico_paquets = dico_paquets_initial(liste_joueurs,map)
    dico_defausses = dico_defausses_initial(liste_joueurs)
    nbr_tour = 1
    if map == 3 :
        derniere_avalanche = 0
    while test_victoire(nb_lignes,taille_seg,dico_positions,terminus) != True :
        if map == 3 :
            derniere_avalanche = tour_de_jeu(nb_lignes,taille_seg,dico_positions,positions_droite,positions_gauche,dico_paquets,dico_defausses,dico_velo,configurations,nbr_tour,map,affichage,derniere_avalanche)
        else : 
            tour_de_jeu(nb_lignes,taille_seg,dico_positions,positions_droite,positions_gauche,dico_paquets,dico_defausses,dico_velo,configurations,nbr_tour,map,affichage)
        afficher_fenetre(affichage_route(nb_lignes,taille_seg,positions_droite,positions_gauche,start,terminus))
        if map == 3 :
            derniere_avalanche += 1 
        nbr_tour += 1
    afficher_fenetre("Le vainqueur de l'étape est " + le_plus_eloigne(dico_positions) + " !!")





